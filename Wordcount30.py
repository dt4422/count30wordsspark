import sys

from pyspark import SparkConf, SparkContext

if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: spark-submit wordcount <file>", file=sys.stderr)
        exit(-1)

    sparkConf = SparkConf()
    sparkContext = SparkContext(conf=sparkConf)

    sparkContext.setLogLevel("OFF")

    output = sparkContext \
        .textFile(sys.argv[1]) \
        .flatMap(lambda line: line.split(' ')) \
        .map(lambda word: (word, 1)) \
        .reduceByKey(lambda a, b: a + b) \
        .sortByKey()


    count1=output.takeOrdered(30,lambda s:-1*s[1])
    for (word, count) in count1:
        print("%s: %i" % (word, count))

    sparkContext.stop()
