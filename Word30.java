
package org.uma.bbdd.PruebaSpark;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;


import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import scala.Tuple2;

/**
 * Example implementing the Spark version of the "Hello World" Big Data program: counting the
 * number of occurrences of words in a set of files.
 *
 * @author Antonio J. Nebro <antonio@lcc.uma.es>
 */

public class Word30 {

	public static void main(String[] args) {
    // Step 1: parameter checking and log configuration
		if (args.length < 1) {
      throw new RuntimeException("Syntax Error: there must be one argument (a file name or a directory)");
		}

		Logger.getLogger("org").setLevel(Level.OFF) ;

		// STEP 2: create a SparkConf object
		SparkConf sparkConf = new SparkConf().setAppName("Spark Word count") ;

		// STEP 3: create a Java Spark context
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf) ;

    List<Tuple2<String, Integer>> output = sparkContext
            .textFile(args[0])
            .flatMap(s -> Arrays.asList(s.split(" ")).iterator())
            .mapToPair(s -> new Tuple2<>(s, 1))
            .reduceByKey((integer, integer2) -> integer + integer2)
            .sortByKey()
            .takeOrdered(30,MyTupleComparator.INSTANCE);
            

		// STEP 4: print the results
		for (Tuple2<?, ?> tuple : output) {
			System.out.println(tuple._1() + ": " + tuple._2()) ;
		}

		// STEP 5: stop the spark context
		sparkContext.stop();
	}
	static class MyTupleComparator implements Comparator<Tuple2<String, Integer>> ,Serializable {
	       final static MyTupleComparator INSTANCE = new MyTupleComparator();
	       @Override
	       public int compare(Tuple2<String, Integer> t1, Tuple2<String, Integer> t2) {
	          return -t1._2.compareTo(t2._2);     // sorts RDD elements descending (use for Top-N)
	          // return t1._2.compareTo(t2._2);   // sorts RDD elements ascending (use for Bottom-N)
	       }
	}
}


